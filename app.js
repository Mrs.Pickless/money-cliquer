var messages = []; //historiques messages
var listPlayers = [];
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var ent = require('ent'); // Permet de bloquer les caractères HTML (sécurité équivalente à htmlentities en PHP)
app.use(express.static(__dirname + '/public'));

app.get('/index.html', function(req, res, next) {
    res.sendFile(__dirname + '/index.html');
});

//Fonction lors de la connexion au serveur pour envoyé les pseudo et le score
io.on('connection', function (socket) {

    messages.forEach(element =>  socket.emit('service-message', element));

    for (let socketid in io.sockets.sockets) {
        let other = io.sockets.sockets[socketid];
        if(other.pseudo !== undefined) {
            socket.emit('enter', other.pseudo, other.id); 
        }
        
    }
//Envoie Message de l'utilisateur a tous les autres utilisateurs
    socket.on('chat-message', function (message , pseudo) {
        let pseudoBold = "<b>" + encode_utf8( pseudo )   + " : </b>" ;
        var messageToSend = pseudoBold  + encode_utf8(message)  ;
        io.emit('chat-message',messageToSend);
        messages.push(messageToSend);
        if (messages.length > 150) {
          messages.splice(0, 1);
        }
 
      });


    //Fonction qui active la connexion au jeu
    socket.on('enter', function(pseudo) {

        listPlayers.push(pseudo);
        socket.pseudo = pseudo;
        console.log(socket.pseudo + " vient de rentrer dans la room id = " + socket.id);
        io.emit('enter', socket.pseudo, socket.id);
            let serviceMessage = '<i>Connexion de l utilisateur  : <b>' + encode_utf8(pseudo) +" </b></i>";
            socket.broadcast.emit('service-messageUTF8', serviceMessage);
            messages.push(serviceMessage);

            io.emit('receiveChatUser', listPlayers.length);
    });


    //Fonction qui envoie le User score du joueur
    socket.on('emitscore', function(userScore) {
        let newlist = [];
        for (let socketid in io.sockets.sockets) {
            let other = io.sockets.sockets[socketid];
            if(other.pseudo !== undefined && other.player !== undefined) {
                newlist.push([other.id, other.pseudo, other.player.userTotalScore, other.player.userScore, other.player.userScorePS]);
            }
        }
        
        newlist.sort((a, b) => (a[2] < b[2]) ? 1 : -1);

        
        io.emit('update_playerlist', newlist);
        io.emit('receivescore', socket.id, Math.round(userScore*100)/100);
    });

    //Fonction qui affiche le score  par seconde du joueur
    socket.on('emitscoreps', function(userScorePS) {
        io.emit('receivescoreps', socket.id, Math.round(userScorePS*100)/100);
    });

    
    socket.on('stoleBuilding', function(thief,victim,building,pseudoVictime ,pseudoVoleur , BuildingName) {
      if(io.sockets.sockets[victim].player.ownedbuildings[building] != undefined){
          let randNbr = Math.floor(Math.random() * 101); 
          let nbrBuilding = io.sockets.sockets[victim].player.ownedbuildings[building][1];
          let nbrStole = (nbrBuilding/100)*randNbr;
          nbrStole = Math.round(nbrStole);
          console.log("Pourcentage volé = " + randNbr + " nbr = " + nbrStole);
          io.emit('stoleBuilding',thief,victim,  nbrStole , building);
          let serviceMessage1 = '<i><b>'+ pseudoVoleur  + '</b> à volé ' + nbrStole +' '+BuildingName +' à <b>'+ pseudoVictime+'</b></i>';
          io.emit('service-messageUTF8', encode_utf8(serviceMessage1));
          messages.push(serviceMessage1);
      }else{
        console.log("le vol a échoué");
        io.emit('failedStole', thief , building );
        let serviceMessageFailStole = '<i><b>'+ pseudoVoleur  + '</b> à raté son vol sur <b>'+ pseudoVictime+'</b></i>';
          io.emit('service-messageUTF8', encode_utf8(serviceMessageFailStole));
          messages.push(serviceMessageFailStole);
      }
      
  });


    socket.on('emittotalscore', function(userTotalScore) {
        io.emit('receivetotalscore', socket.id, Math.round(userTotalScore*100)/100);
    });

    socket.on('sendplayer', function(player) {
        socket.player = player;
    });


    socket.on('disconnect', function() {
        if (socket.pseudo !== undefined) {
            var serviceMessage2 = '<i>Déconnexion de l utilisateur  : <b>' + socket.pseudo +"</b></i>";
            socket.broadcast.emit('service-messageUTF8', encode_utf8(serviceMessage2));
            messages.push(serviceMessage2);
            listPlayers.shift();
            io.emit('receiveChatUser', listPlayers.length);
          }
    });

    //fonction déconnexion
    socket.on('disconnect', function() {
        if(socket.pseudo != undefined){

        //    let serviceMessage = '<i>Déconnexion de l utilisateur  : <b>' + socket.pseudo +" </b></i>";
        //    socket.broadcast.emit('service-message', serviceMessage);
        //    console.log(serviceMessage);
        //    messages.push(serviceMessage);
         //   console.log(messages);
            socket.broadcast.emit('leave', socket.id , socket.pseudo);
            console.log("Deconnexion de l'utilisateur id = " + socket.id +' Pseudo = ' + socket.pseudo);
        }
     });
    
});


server.listen(8080);
  
function encode_utf8(s) {
    return unescape(encodeURIComponent(s));
  }
  
  function decode_utf8(s) {
    return decodeURIComponent(escape(s));
  }