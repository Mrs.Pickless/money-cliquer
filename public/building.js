class Building {
    static buildings = [];

    constructor(name, fullname, desc , price , scorePS, appear) {
        this.name = name;
        this.fullname = fullname;
        this.desc = desc;
        this.price = price;
        this.scorePS = scorePS;
        this.appear = appear;
    }
}

