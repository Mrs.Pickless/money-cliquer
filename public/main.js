    var poubelle = new Building("Poubelle", "Poubelle", "Vous fouillez les poubelles", 15, 1, 0);
    var mineur = new Building("mineur", "Mineur", "Mains d'oeuvre à bas prix", 100, 4, 0);
    var cafe = new Building("Machine_a_cafe", "Machine à café", "Un petit café ?", 1000, 10, 15);
    Building.buildings.push(poubelle, mineur, cafe);

    var poubelleX2 = new Upgrade("PoubelleX2" , "Poubelle x2" , "Vous trouvez 2 fois plus dans vos poubelles" ,  "assets/img/poubelleX2.png" , 15 , 0, 15);
    var mineurX2 = new Upgrade("MineurX2" , "Mineur x2" , "Vos employées travaillent 2 fois plus" ,  "assets/img/mineurX2.png" , 150 , 1, 150);
    Upgrade.upgrades.push(poubelleX2 , mineurX2);

    var volpoubelle = new Malus("volpoubelle" , "Vol de poubelles" ,"volez un nombre aléatoire de poubelle a vos adversaires" ,"assets/img/poubelleX2.png", 20 ,0 , 25 );
    var volmineur = new Malus("volmineur" , "Vol de mineurs" ,"volez un nombre aléatoire de mineurs a vos adversaires" ,"assets/img/mineurX2.png", 40 ,1 , 30 );
    Malus.malus.push(volpoubelle , volmineur);

    jQuery.fn.exists = function(){ return this.length > 0; }
    

    setInterval(showBuildingsAndUpgrades, 100);

    //Initialiser les batiments et les upgrades de base si ils peuvents l'être via la appear
    function showBuildingsAndUpgrades() {


        for(var i = 0; i < Building.buildings.length; i++) {
            if(Building.buildings[i].appear <=  self.userTotalScore && $("#" + Building.buildings[i].name + "_li").exists() == false  ){
            var string = "\
                <li class='list-group-item impossibleUpgrade' style='cursor: cell;' id='" + Building.buildings[i].name + "_li'  data-toggle='tooltip' title='???' onclick=\"self.buy('"+ i +"')\">\
                    <div class='row'>\
                        <div class='col-sm-4'><center id=\"" + Building.buildings[i].name + "_name\">???</center></div>\
                        <div class='col-sm-4'><center id=\"" + Building.buildings[i].name + "_price\">???</center></div>\
                        <div class='col-sm-4'><center><span class='badge badge-dark badge-pill' id=\"" + Building.buildings[i].name + "_quantity\">???</span></center></div>\
                    </div>\
                </li>\
            ";
            
            $("#marketlist").append(string);
            }
        }


    

     //Afficher les upgrades
    for(var i = 0; i < Upgrade.upgrades.length; i++) {
        if(Upgrade.upgrades[i].appear <=  self.userTotalScore && self.ownedupgrades[i] == undefined && $("#" + Upgrade.upgrades[i].name + "_li").exists() == false  ){
        var string = "\
            <li class='list-group-item impossibleUpgrade' style='cursor: cell;' id='" + Upgrade.upgrades[i].name + "_li'  data-toggle='tooltip' title='???' onclick=\"self.buyUpgrade('"+ i +"')\">\
                <div class='row'>\
                    <div class='col-sm-6' id=\"" + Upgrade.upgrades[i].name + "_name\"><center>???</center></div>\
                    <div class='col-sm-6' id=\"" + Upgrade.upgrades[i].name + "_price\"><center>???</center></div>\
                </div>\
            </li>\
        ";
        
        $("#upgradelist").append(string);
        }
    }

         //Afficher les malus onclick=\"self.buyMalus('"+ i +"')\"
         for(var i = 0; i < Malus.malus.length; i++) {
            if(Malus.malus[i].appear <=  self.userTotalScore && self.ownedmalus[i] == undefined && $("#" + Malus.malus[i].name + "_li").exists() == false  ){
                var string = "\
                <li class='list-group-item impossibleUpgrade no-modal' style='cursor: cell;' id='" + Malus.malus[i].name + "_li'  data-toggle='tooltip' title='???' \"   >\
                    <div class='row'  data-toggle='modal'  data-target='#MalusModal_" + i +"' >\
                        <div class='col-sm-6' id=\"" + Malus.malus[i].name + "_name\"><center>???</center></div>\
                        <div class='col-sm-6' id=\"" + Malus.malus[i].name + "_price\"><center>???</center></div>\
                    </div>\
                </li>\
                ";
                var string2= "\
                <div class='modal fade' id=\"MalusModal_"  + i +"\" tabindex='-1' role='dialog' aria-labelledby='MalusModal' aria-hidden='true'>\
                <div class='modal-dialog' role='document'>\
                    <div class='modal-content'>\
                    <div class='modal-header'>\
                        <h5 class='modal-title' style='color: black;' id='exampleModalLabel'>Choisir un adversaire à voler</h5>\
                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>\
                        <span aria-hidden='true'>&times;</span>\
                        </button>\
                    </div>\
                    <div class='modal-body'>\
                    <input id='typeMalusSend' type='hidden' value=''>\
                        <div id=\"modalPseudoList_" + i + "\" class='list-group' data-malus=\""+ i +"\">\
                        </div>\
                        <div id=\"modalNobody_" + i + "\" >\
                        </div>\
                    </div>\
                    </div>\
                </div>\
                </div>\
                ";
                $("#modalList").append(string2);
                $("#maluslist").append(string);
            }
        }

}