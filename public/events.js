var idJoueur ;
var idVoleur ;
var audio0 = new Audio("assets/Sound/impactMetal_heavy_000.ogg");
var audio1 = new Audio("assets/Sound/impactMetal_heavy_001.ogg");
var audio2 = new Audio("assets/Sound/impactMetal_heavy_002.ogg");
var audio3 = new Audio("assets/Sound/impactMetal_heavy_003.ogg");
var audio4 = new Audio("assets/Sound/impactMetal_heavy_004.ogg");
var malus0 = new Audio("assets/Sound/malus0.mp3");
var malus1 = new Audio("assets/Sound/malus1.mp3");
audio0.volume = 0.2;
audio1.volume = 0.2;
audio2.volume = 0.2;
audio3.volume = 0.2;
audio4.volume = 0.2;
malus0.volume  = 0.8;
malus1.volume  =0.8;


$( "#coin" ).click(function() {
  self.addScore(self.clickPoint);
  var x = Math.floor(Math.random() * 101); 
  if(x < 40 ){
    audio0.play();
  }else if(x < 60 ){
    audio4.play();
  }else if(x < 75 ){
    audio3.play();
  }else if(x < 80 ){
    audio2.play();
  }else{
    audio1.play();
  }

});



function encode_utf8(s) {
    return unescape(encodeURIComponent(s));
  }
  
  function decode_utf8(s) {
    return decodeURIComponent(escape(s));
  }

  var reg=/^[a-z-Z0-9\s]*$/i
  
      /**
   * Scroll vers le bas de page si l'utilisateur n'est pas remonté pour lire d'anciens messages
   */
   function scrollToBottom() {
    var elem = document.getElementById('messages');
    elem.scrollTop = elem.scrollHeight; }
  
      var socket = io.connect();
  
      var pseudo = prompt('Quel est votre pseudo ?');//entrer le pseudo
      
  
      //Si le pseudo est nul alors on refresh la page
      if (pseudo === null || pseudo == '' || pseudo.length > 11 || reg.test(pseudo) == false ) {
          location.reload();
      } else {


        socket.emit('enter', pseudo);
  
        socket.on('service-message', function(msg) {
          $('#messages').append($('<li>').html(msg));
            scrollToBottom();
        });
        socket.on('service-messageUTF8', function(msg) {
          $('#messages').append($('<li>').html(decode_utf8(msg)));
            scrollToBottom();
        });
        
        socket.on('receiveChatUser', function(nbr) {
          $('#playerNbr').text(nbr);
        });
  
        socket.on('enter', function(pseudo, id) {
          var string = "\
              <li class='list-group-item' id='" + id + "_li'>\
                  <div class='row'>\
                      <div class='col-sm-3'><center id=\"" + id + "_pseudo\">"+ pseudo +"</center></div>\
                      <div class='col-sm-3'><center id=\"" + id + "_score\">0</center></div>\
                      <div class='col-sm-3'><center id=\"" + id + "_scoreps\">0</center></div>\
                      <div class='col-sm-3'><center id=\"" + id + "_totalscore\">0</center></div>\
                  </div>\
              </li>\
          ";
          $('#playerlist').append(string);
        });
  
        socket.on('receivescore', function(id, score) {
          $('#' + id + '_score').text(score);
        });
  
        socket.on('receivescoreps', function(id, score) {
          $('#' + id + '_scoreps').text(score);
        });
  
        socket.on('receivetotalscore', function(id, score) {
  
          $('#' + id + '_totalscore').text(score);
        });

        function isEmpty( el ){
          return !$.trim(el.html())
      }
  //ajout des joueur dans la lisste avec leurs scores
        socket.on('update_playerlist', function(list) {
          idJoueur = socket.io.engine.id;
          $('#playerlist').empty();
          for(var j = 0; j < Malus.malus.length; j++) {
                $('#modalPseudoList_' + j).empty();
                // var malus = $('#modalPseudoList_' + i).attr("data-malus");
          }

          // if soocket.id = list[i][0] alors ne pas mettre dans modalPSeudolist
          for(var i = 0; i < list.length; i++) {
            if(list.length == 1 ){
              var yourSolo = true ;
            }else {
              var yourSolo = false ;
            }
           // console.log(list[i][0] + ' | ' + list[i][1]);
            var string = "\
              <li class='list-group-item' id='" + list[i][0] + "_li'>\
                  <div class='row'>\
                      <div class='col-sm-3'><center id=\"" + list[i][0] + "_pseudo\">"+ list[i][1] +"</center></div>\
                      <div class='col-sm-3'><center id=\"" + list[i][0] + "_score\">"+ Math.round(list[i][3]*100)/100 +"</center></div>\
                      <div class='col-sm-3'><center id=\"" + list[i][0] + "_scoreps\">"+ Math.round(list[i][4]*100)/100 +"</center></div>\
                      <div class='col-sm-3'><center id=\"" + list[i][0] + "_totalscore\">"+ Math.round(list[i][2]*100)/100 +"</center></div>\
                  </div>\
              </li>\
            ";
            $('#playerlist').append(string);
                //socket.id , list[i][0]} , j onclick='FuncSendMalus(${list[i][0]},${idJoueur},${j})'
                for(var j = 0; j < Malus.malus.length; j++) {
                  let malusRef = Malus.malus[j].buildingRef ;
                  let buildingName =  Building.buildings[malusRef].name ;
                  if(yourSolo == true && isEmpty($('#modalNobody_' + j)) ){
                    var pSolo = `<p class="alert alert-danger" style='color:black;'><b>Et non ${pseudo} !</b><br> Tu ne peux pas volé de ${buildingName} aujourd'hui car tu joue seul et tu n'a pas d'amis !</p>`;
                    $('#modalNobody_' + j).append(pSolo);
                  }
                  if(yourSolo == false ){
                    $('#modalNobody_' + j).empty();
                  }
                  if(list[i][0] !== socket.io.engine.id){
                  idVictim = list[i][0] ;
                  let pseudoVictim = list[i][1] ;
                  var idDiv = "#"+ list[i][0] +"_MalusPseudo" + j ;
                    var string2 = `
                    <button type='button' id='${list[i][0]}_MalusPseudo${j}'  data-building='${j}' data-dismiss="modal" target="_blank"  class='list-group-item list-group-item-action'> ${list[i][1]} </button>
                    `;
                    $('#modalPseudoList_' + j).append(string2)
                    // var malus = $('#modalPseudoList_' + i).attr("data-malus");
                    $(idDiv ).click(function () {
                      socket.emit('stoleBuilding',idJoueur , idVictim , $(this).attr("data-building") ,pseudoVictim , pseudo , buildingName  );                
                      self.buyMalus($(this).attr("data-building"));
                      //function qui ferme le modal
                      // function qui suprimme le modal et ces div 
                      

                    });
                }
              }


           
            // $('#modalPseudoList_1').append(string2);
            // $('#modalPseudoList_2').append(string2);
          }

        });
        socket.on('stoleBuilding' , function(thief,victim,  nbrStole , type){
          var x = Math.floor(Math.random() * 101); 
          if(x < 50 ){
            malus0.play();
          }else{
            malus1.play();
          }
          if (socket.io.engine.id == thief ){
        // on augmente le prix de prix*50
        Malus.malus[type].price = Malus.malus[type].price * 50 ;
        $('#' + Malus.malus[type].name + '_price').text(Malus.malus[type].price);
        $('#' + Building.buildings[type].name + '_li').removeClass('upgrade').addClass('impossibleUpgrade');
        $('#' + Malus.malus[type].name + '_li').addClass('no-modal');
            if(self.ownedbuildings[type] !== undefined) {
              self.ownedbuildings[type][1] = self.ownedbuildings[type][1] + nbrStole ;
              self.userScorePS = self.userScorePS + (Building.buildings[type].scorePS * nbrStole);
          } else {
              self.ownedbuildings[type] = [Building.buildings[type], nbrStole];
              self.userScorePS = self.userScorePS + (Building.buildings[type].scorePS * nbrStole);
          }
          }
          if (socket.io.engine.id == victim ){
            self.ownedbuildings[type][1] = ( self.ownedbuildings[type][1] - nbrStole ); 
            self.userScorePS = self.userScorePS - (Building.buildings[type].scorePS * nbrStole);
          }
        });


        socket.on('failedStole' , function(thief, type){
          var x = Math.floor(Math.random() * 101); 
          if(x < 50 ){
            malus0.play();
          }else{
            malus1.play();
          }
          if (socket.io.engine.id == thief ){
            // on augmente le prix de prix*50
            Malus.malus[type].price = Malus.malus[type].price * 50 ;
            $('#' + Malus.malus[type].name + '_price').text(Malus.malus[type].price);
            $('#' + Building.buildings[type].name + '_li').removeClass('upgrade').addClass('impossibleUpgrade');
            $('#' + Malus.malus[type].name + '_li').addClass('no-modal');
          }
        }); 
  
        socket.on('leave', function(id ,pseudo) {
          $('#' + id + '_li').remove();
        });
  
  
      }
  
  