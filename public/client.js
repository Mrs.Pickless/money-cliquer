class Client {
    userScore = 0;
    userTotalScore = 0;
    userScorePS = 0;
    clickPoint = 1;

    ownedbuildings = [];
    ownedupgrades = [];
    ownedmalus = [];

    constructor() {
        document.title = "0$ Money Clicker";
        $('#userScorePS').text(this.userScorePS);
        var that = this;

        // Augmenter le score toutes les secondes en fonction du userScorePS 
        setInterval(function() { 
            that.addScore(that.userScorePS);
        }, 1000)
        //

        // Afficher le scorePS toutes les 100 millisecondes
        setInterval(that.showScorePS, 10, that);
        //
    }

    showScorePS(that) {
        document.title = Math.round(that.userScore*100)/100  + "$ Money Clicker";
        $('#userScorePS').text(Math.round(that.userScorePS*100)/100);
        socket.emit('emitscoreps', Math.round(that.userScorePS*100)/100);

        for(var i = 0; i < Building.buildings.length; i++) {
            
            if(that.userTotalScore >= Building.buildings[i].price || that.ownedbuildings[i] !== undefined) {
                $('#' + Building.buildings[i].name + '_name').text(Building.buildings[i].fullname);
                $('#' + Building.buildings[i].name + '_price').text(Math.round(Building.buildings[i].price*100)/100);
                if(that.ownedbuildings[i] !== undefined) {
                    $('#' + Building.buildings[i].name + '_quantity').text(that.ownedbuildings[i][1]);
                } else {
                    $('#' + Building.buildings[i].name + '_quantity').text(0);
                }
                $('#' + Building.buildings[i].name + '_li').attr('data-original-title', Building.buildings[i].desc);
                $('[data-toggle="tooltip"]').tooltip();

                if(that.userScore >= Building.buildings[i].price) {
                    $('#' + Building.buildings[i].name + '_li').removeClass('impossibleUpgrade').addClass('upgrade');
                    //Projet couleur sur prix  $('#' + Building.buildings[i].price + '_li').style.color = '#FF0000';;
                } else {
                    $('#' + Building.buildings[i].name + '_li').removeClass('upgrade').addClass('impossibleUpgrade');
                    //Projet couleur sur prix  $('#' + Building.buildings[i].price + '_li').style.color = '#2ecc71';
                }
            }
            
        }

        for(var i = 0; i < Upgrade.upgrades.length; i++) {
            if(that.userTotalScore >= Upgrade.upgrades[i].price || that.ownedupgrades[i] !== undefined) {
                $('#' + Upgrade.upgrades[i].name + '_name').text(Upgrade.upgrades[i].fullname);
                $('#' + Upgrade.upgrades[i].name + '_price').text(Upgrade.upgrades[i].price);
                $('#' + Upgrade.upgrades[i].name + '_li').attr('data-original-title', Upgrade.upgrades[i].desc);
                
                
                if(that.userScore >= Upgrade.upgrades[i].price && that.ownedbuildings[Upgrade.upgrades[i].buildingRef] !== undefined) {
                    $('#' + Upgrade.upgrades[i].name + '_li').removeClass('impossibleUpgrade').addClass('upgrade');
                    //Projet couleur sur prix  $('#' + Building.buildings[i].price + '_li').style.color = '#FF0000';;
                } else {
                    $('#' + Upgrade.upgrades[i].name + '_li').removeClass('upgrade').addClass('impossibleUpgrade');
                    //Projet couleur sur prix  $('#' + Building.buildings[i].price + '_li').style.color = '#2ecc71';
                }

                if(that.ownedupgrades[i] !== undefined) $('#' + Upgrade.upgrades[i].name + '_li').remove();
                $('[data-toggle="tooltip"]').tooltip();
            }

        }

        for(var i = 0; i < Malus.malus.length; i++) {
            if(that.userTotalScore >= Malus.malus[i].price ) {
                $('#' + Malus.malus[i].name + '_name').text(Malus.malus[i].fullname);
                $('#' + Malus.malus[i].name + '_price').text(Malus.malus[i].price);
                $('#' + Malus.malus[i].name + '_li').attr('data-original-title',Malus.malus[i].desc);
                
              
                if(that.userScore >=  Malus.malus[i].price) {
                    $('#' + Malus.malus[i].name + '_li').removeClass('impossibleUpgrade').addClass('upgrade');
                    $('#' + Malus.malus[i].name + '_li').removeClass('no-modal');
                    //Projet couleur sur prix  $('#' + Building.buildings[i].price + '_li').style.color = '#FF0000';;
                } else {
                    $('#' + Malus.malus[i].name + '_li').removeClass('upgrade').addClass('impossibleUpgrade');
                    $('#' + Malus.malus[i].name + '_li').addClass('no-modal');
                    //Projet couleur sur prix  $('#' + Building.buildings[i].price + '_li').style.color = '#2ecc71';
                }

                //if(that.ownedmalus[i] !== undefined) $('#' + Malus.malus[i].name + '_li').remove();
                $('[data-toggle="tooltip"]').tooltip();

            }
        }


        socket.emit('emitsownedbuilding', that.ownedbuildings);
        socket.emit("sendplayer", that);
    }

    //Ajoute les Points au userScore et UserTotalScore
    addScore(value) {
        this.userScore = this.userScore + value; 
        this.userTotalScore = this.userTotalScore + value;
        $('#score').text(Math.round(this.userScore*100)/100);
        socket.emit('emitscore', Math.round(this.userScore*100)/100);
        socket.emit('emittotalscore', Math.round(this.userTotalScore*100)/100);
    }

    //Fonction qui si le score est superieur au prix du batiments indiqué soustrait l'argent et l'ajoute aux batiments possédé
    buy(type) {
        if(this.userScore < Building.buildings[type].price) return;

        this.userScore = this.userScore - Building.buildings[type].price;

        if(this.ownedbuildings[type] !== undefined) {
            this.ownedbuildings[type][1] = this.ownedbuildings[type][1] + 1
        } else {
            this.ownedbuildings[type] = [Building.buildings[type], 1];
        }

        let multiplicator = 1;
        
        for(let i = 0; i < this.ownedupgrades.length; i++) {
            console.log("I : " + i)
            console.log(Upgrade.upgrades[i].buildingRef + " ET " + type);
            if(Upgrade.upgrades[i].buildingRef == type) {
                multiplicator = multiplicator + 1;
            } 
        }

        this.userScorePS = this.userScorePS + (Building.buildings[type].scorePS * multiplicator);
        Building.buildings[type].price = Math.round(Building.buildings[type].price * 1.2);
        
    }


    //Fonction qui ajoute des upgrades
    buyUpgrade(type) {
        if(this.userScore < Upgrade.upgrades[type].price || this.ownedbuildings[Upgrade.upgrades[type].buildingRef] === undefined) return;

        this.userScore = this.userScore -  Upgrade.upgrades[type].price;
        $('#' + Upgrade.upgrades[type].name + '_li').tooltip('hide');

        if(this.ownedupgrades[type] !== undefined) {
            this.ownedupgrades[type][1] = this.ownedupgrades[type][1] + 1
        } else {
            this.ownedupgrades[type] = [Upgrade.upgrades[type], 1];
        }

        for(let i = 0; i < this.ownedbuildings[Upgrade.upgrades[type].buildingRef][1]; i++) {
            this.userScorePS = this.userScorePS + Building.buildings[Upgrade.upgrades[type].buildingRef].scorePS;
        }
        
    }


    buyMalus(type){
        if(this.userScore < Malus.malus[type].price ) return;
        
        this.userScore = this.userScore -  Malus.malus[type].price;
        //$('#' + Malus.malus[type].name + '_li').tooltip('hide');

        if(this.ownedmalus[type] !== undefined) {
            this.ownedmalus[type][1] = this.ownedmalus[type][1] + 1 ;
        } else {
            this.ownedmalus[type] = [Malus.malus[type], 1];
        }
        for(let i = 0; i < this.ownedupgrades.length; i++) {
            console.log(Upgrade.upgrades[i].buildingRef + " ET " + type);

        }
        //console.log("LOL " + type);
    }


}

const self = new Client();