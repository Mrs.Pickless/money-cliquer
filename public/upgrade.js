class Upgrade {
    static upgrades = [];

    constructor(name, fullname, desc , logo , price , buildingRef, appear) {
        this.name = name;
        this.fullname = fullname;
        this.desc = desc;
        this.logo = logo;
        this.price = price;
        this.buildingRef = buildingRef;
        this.appear = appear;
    }
}

